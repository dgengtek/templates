import click
# http://click.pocoo.org/5/options/

@click.group("template")
@click.pass_context
def main(ctx, args=None):
    d = dict()
    ctx.obj = d

@click.group("sub")
@click.pass_context
def main2(ctx, args=None):
    d = dict()
    ctx.obj = d

@main.command("test")
@click.argument("filename", required=True)
@click.option("-f","--flag", is_flag=True, help="Flag.")
@click.option('--shout/--no-shout', default=False)
@click.option("-n","--number", default=20, help="Number")
@click.option('--upper', 'transformation', flag_value='upper',
              default=True) # feature switch
@click.option('--lower', 'transformation', flag_value='lower')
@click.option('--hash-type', type=click.Choice(['md5', 'sha1']))
@click.option('--name', prompt=True)
@click.option('--password', prompt=True, hide_input=True,
              confirmation_prompt=True) # or @click.password_option()
@click.pass_obj
def test(d, filename, flag, shout, number, transformation, hash_type, name, password):
    print("test: filename:{}, flag:{}, number:{}".format(filename, flag, number))

@main2.command("subtest")
@click.argument("filename", required=True)
@click.pass_obj
def test2(d, filename):
    print("test2: filename:{}".format(filename))

# add click subcommand from other module
#   which has grouped other subcommands
# main.add_command(clicksubcommand.main)
main.add_command(main2)

if __name__ == "__main__":
    main()
